# Programmer Assistant HASS Add-on: Unbound

Unbound is a validating, recursive, caching DNS resolver.
It is designed to be fast and lean and incorporates modern features based on open standards.

This addon is preconfigured to run as a recursive DNS resolver for Pi-hole.

## Installation

The installation of this add-on is pretty straightforward and not different in
comparison to installing any other Home Assistant add-on.

1. Search for the "unbound" add-on in the Supervisor add-on store and install it.
1. Start the "unbound" add-on.
1. Check the logs of the "unbound" to see is everything OK.
1. Run `dig pi-hole.net @127.0.0.1 -p 5335` where `127.0.0.1` is your homeassistant ip.
You should see something like below:

```
; <<>> DiG 9.16.25 <<>> pi-hole.net @127.0.0.1 -p 5335
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45972
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;pi-hole.net.			IN	A

;; ANSWER SECTION:
pi-hole.net.		300	IN	A	3.18.136.52

;; Query time: 92 msec
;; SERVER: 127.0.0.1#5335(127.0.0.1)
;; WHEN: Wed Feb 16 16:20:23 UTC 2022
;; MSG SIZE  rcvd: 56
```

## Configuration

Nothing to configure here.

## Changelog & Releases

This repository keeps a change log using [CHANGELOG.md](CHANGELOG.md)
functionality.

Releases are based on [Semantic Versioning][semver], and use the format
of `MAJOR.MINOR.PATCH`. In a nutshell, the version will be incremented
based on the following:

- `MAJOR`: Incompatible or major changes.
- `MINOR`: Backwards-compatible new features and enhancements.
- `PATCH`: Backwards-compatible bugfixes and package updates.

## Support

Got questions?

[Open an issue here][issue] on GitLab.

## Authors & contributors

The original setup of this repository is by [Dawid Rycerz][knightdave].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

MIT License

Copyright (c) 2021-2022 Dawid Rycerz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

[semver]: http://semver.org/spec/v2.0.0.html
[issue]: https://gitlab.com/programmer-assistant/addons/-/issues
[contributors]: https://gitlab.com/programmer-assistant/addons/-/graphs/master
[knightdave]: https://gitlab.com/knightdave
