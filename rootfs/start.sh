#!/bin/bash

if [[ ! -d "/data/unbound" ]]; then
    echo ":::Initializing unbound configuration on persistent storage"
    mkdir -p /data/unbound
    cp -R /etc/unbound/* /data/unbound
fi

echo ":::Running chroot_setup"
/usr/lib/unbound/package-helper chroot_setup

echo ":::Running root_trust_anchor_update"
/usr/lib/unbound/package-helper root_trust_anchor_update

echo ":::Starting unbound"
chmod 0666 /dev/stdout
exec /usr/sbin/unbound -d -c /data/unbound/unbound.conf
