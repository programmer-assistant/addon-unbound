# Changelog

# [0.4.0](https://gitlab.com/programmer-assistant/addon-unbound/compare/v0.3.0...v0.4.0) (2022-02-16)


### :bug:

* Fix so-rcvbuf error ([294058a](https://gitlab.com/programmer-assistant/addon-unbound/commit/294058a61864fc72bd7e3c74d86c177893345178))

# [0.3.0](https://gitlab.com/programmer-assistant/addon-unbound/compare/v0.2.0...v0.3.0) (2022-02-16)


### :bug:

* Fix so-rcvbuf error ([1b4cac8](https://gitlab.com/programmer-assistant/addon-unbound/commit/1b4cac82171fb5d95d2e2e18843cdf94be71b013))

# [0.2.0](https://gitlab.com/programmer-assistant/addon-unbound/compare/v0.1.0...v0.2.0) (2022-02-16)


### :bug:

* Fix config.json ([f20d97f](https://gitlab.com/programmer-assistant/addon-unbound/commit/f20d97fd29562e65855d68d1a43a07de35436297))

## 0.1.0

- Initial build based on unbound 1.13.1
