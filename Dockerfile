ARG DEP_PROXY=
FROM ${DEP_PROXY}debian:11-slim

ENV CONFIG_PATH /data/options.json

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        bash=* \
        jq=* \
        unbound=* \
        dns-root-data=* \
        dnsutils=* \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /var/lib/unbound

COPY rootfs /

EXPOSE 5335/tcp
EXPOSE 5335/udp

ENTRYPOINT ["/start.sh"]

ARG BUILD_DATE
ARG BUILD_DESCRIPTION
ARG BUILD_NAME
ARG BUILD_REF
ARG BUILD_VERSION

# Labels
LABEL \
    io.hass.name="${BUILD_NAME}" \
    io.hass.description="${BUILD_DESCRIPTION}" \
    io.hass.arch="multiarch" \
    io.hass.type="addon" \
    io.hass.version=${BUILD_VERSION} \
    maintainer="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.title="${BUILD_NAME}" \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.vendor="Programmer Assistant HASS Add-on" \
    org.opencontainers.image.authors="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://addons.programmer-assistant.io" \
    org.opencontainers.image.source="https://gitlab.com/programmer-assistant/addons/-/tree/main/${BUILD_NAME}" \
    org.opencontainers.image.documentation="https://gitlab.com/programmer-assistant/addons/-/blob/main/${BUILD_NAME}/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}
